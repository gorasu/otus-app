FROM php:7.4-cli as build
ARG username=app
ARG apcu_version=5.1.18

RUN apt-get update && apt-get install -y \
    libpq-dev \
     --no-install-recommends git-core openssh-client unzip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --version=1.8.4 --filename=composer


RUN groupadd $username && useradd -m $username -g $username

RUN docker-php-ext-install pdo pdo_pgsql pdo_mysql mysqli

RUN pecl install apcu-${apcu_version} && docker-php-ext-enable apcu
#RUN echo "extension=apcu.so" >> /usr/local/etc/php/php.ini
RUN echo "apc.enable_cli=1" >> /usr/local/etc/php/php.ini
RUN echo "apc.enable=1" >> /usr/local/etc/php/php.ini

COPY ./app /usr/src/myapp
RUN chown -R $username:$username /usr/src/myapp

WORKDIR /usr/src/myapp

EXPOSE 8000
USER $username
ENV APP_ENV=prod
RUN composer install --no-dev --no-scripts --prefer-dist --optimize-autoloader
RUN php bin/console cache:clear

WORKDIR /usr/src/myapp/public

CMD ["php", "-S", "0.0.0.0:8000"]
