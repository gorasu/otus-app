<?php


namespace App\Controller\V1;


use App\Exception\ApiControllerException;
use App\Model\User\Entity\User;
use App\Model\User\Exception\UserNotExistsException;
use App\Model\User\Service\CreateUser;
use App\Model\User\Service\DeleteUser;
use App\Model\User\Service\GetUser;
use App\Model\User\Service\UpdateUser;
use App\Model\User\Service\UserConverter;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PDO;
use PDOException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserController
 * @package App\Controller\V1
 * @Route("/user")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function actionIndex(Request $request, EntityManagerInterface $entityManager){



        return $this->json(['status'=>'ok',

            [

                'update'=> $this->generateUrl('app_v1_user_actionupdate',['user_id'=>'%0%'], UrlGeneratorInterface::ABSOLUTE_URL),
                'delete'=> $this->generateUrl('app_v1_user_actiondelete',['user_id'=>'%0%'], UrlGeneratorInterface::ABSOLUTE_URL),
                'get'=> $this->generateUrl('app_v1_user_actionget',['user_id'=>'%0%'], UrlGeneratorInterface::ABSOLUTE_URL),
                'create'=> $this->generateUrl('app_v1_user_actioncreate',['email'=>'','firstName'=>'','lastName'=>''], UrlGeneratorInterface::ABSOLUTE_URL),

            ]

            ]);
    }

    /**
     * @Route("/create", methods={"POST"})
     */
    function actionCreate(Request $request, CreateUser $createUser){

        try {
            $user = $createUser->fromRequest($request);
            return $this->json(['status'=>'ok', 'id'=>$user->getId()->getValue()]);
        }catch (Exception $e) {
            throw new ApiControllerException($e->getMessage());
        }
    }

    /**
     * @Route("/delete/{user_id}", methods={"DELETE"})
     */
    public function actionDelete($user_id, DeleteUser $deleteUser){
        try {
            $deleteUser->byId($user_id);
            return $this->json(['status'=>'ok']);
        }
        catch (Exception $e) {
            throw new ApiControllerException($e->getMessage());
        }

    }


    /**
     * @Route("/update/{user_id}", methods={"PUT"})
     * @param $user_id
     * @param UpdateUser $updateUser
     * @param Request $request
     * @throws ApiControllerException
     */
    public function actionUpdate($user_id, UpdateUser $updateUser, Request $request){
        try {
            $updateUser->fromRequest($user_id, $request);
            return $this->json(['status'=>'ok']);
        }
        catch (Exception $e) {
            throw new ApiControllerException($e->getMessage());
        }
    }

    /**
     * @Route("/{user_id}", methods={"GET"})
     *
     */
    public function actionGet(string $user_id, GetUser $getUser, UserConverter $converter){


        try {
            $userDto = $converter->toDto($getUser->byId($user_id));

            return $this->json($userDto);
        } catch (Exception $e){

            throw new ApiControllerException($e->getMessage());
        }


    }


    /**
     * @Route("/credentials", methods={"POST"})
     */
    public function actionCredentials(Request $request, UserPasswordEncoderInterface $passwordEncoder, GetUser $getUser, UserConverter $userConverter)
    {
      try {

          $user = $getUser->byEmail($request->get('login'));
          if($passwordEncoder->isPasswordValid($user, $request->get('password'))){
              $userDto = $userConverter->toDto($user);
              return $this->json($userDto);
          }

          throw new Exception("NoUser");



      }catch (Exception $exception){
          throw new ApiControllerException($exception->getMessage());
      }

    }



}