<?php


namespace App\Model\User\Service;


use App\Model\User\Entity\UserId;
use App\Model\User\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mime\Encoder\EncoderInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUser
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var EncoderInterface
     */
    private $encoder;

    function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        $this->entityManager = $entityManager;
        $this->encoder = $encoder;
    }

    /**
     * @param Request $request
     * @return User
     */
    function fromRequest(Request $request){


         return $this->createUser($request->get('email'),
            $request->get('firstName'),
            $request->get('lastName'),
            $request->get('password') );



    }

    /**
     * @param $array
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    function fromArray($array){

        return $this->createUser($array['email'],$array['firstName'],$array['lastName'], $array['password']  );

    }

    private function createUser($email, $firstName, $lastName, $password ){

        $user = new User(UserId::create(), $email);
        $passwordHash = $this->encoder->encodePassword($user, $password);

        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setPasswordHash($passwordHash);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return $user;

    }

}