<?php


namespace App\Model\User\Service;


use App\Model\User\Entity\User;
use App\Model\User\Exception\UserNotExistsException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class DeleteUser
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    function byId($userId){


        $user = $this->entityManager->getRepository(User::class)->find($userId);
        if(!$user){
            throw new UserNotExistsException('User '.$userId.' does not exists.');
        }
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

}