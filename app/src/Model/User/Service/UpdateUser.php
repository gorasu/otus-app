<?php


namespace App\Model\User\Service;


use App\Model\User\Entity\User;
use App\Model\User\Exception\UserNotExistsException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class UpdateUser
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    function fromRequest($userId,Request $request){

        $user = $this->entityManager->getRepository(User::class)->find($userId);
        if(!$user){
            throw new UserNotExistsException('User '.$userId.' does not exists.');
        }
        $user->setFirstName($request->get('firstName'));
        $user->setLastName($request->get('lastName'));
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

}