<?php


namespace App\Model\User\Service;


use App\Model\User\DTO\UserDto;
use App\Model\User\Entity\User;
use App\Model\User\Exception\UserNotExistsException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class UserConverter
{




    public function toDto(User $user){

        $userDto = new UserDto();
        $userDto->lastName = $user->getLastName();
        $userDto->firstName = $user->getFirstName();
        $userDto->id = $user->getId()->getValue();
        $userDto->email = $user->getEmail();
        return $userDto;


    }

}