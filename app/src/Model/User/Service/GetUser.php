<?php


namespace App\Model\User\Service;


use App\Model\User\DTO\UserDto;
use App\Model\User\Entity\User;
use App\Model\User\Exception\UserNotExistsException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class GetUser
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


     function byId($userId): User{

        $user = $this->entityManager->getRepository(User::class)->find($userId);
        if(!$user){
            throw new UserNotExistsException('User '.$userId.' does not exists.');
        }

        return $user;

    }

     function byEmail($email): User{

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email'=>$email]);
        if(!$user){
            throw new UserNotExistsException('User '.$email.' does not exists.');
        }

        return $user;

    }
}