<?php


namespace App\Model\User\Entity;



use Ramsey\Uuid\Uuid;
use Webmozart\Assert\Assert;

class UserId
{
    private $value;

    public function __construct(string $value)
    {
        Assert::notEmpty($value);
        $this->value = $value;
    }

    public static function create(): self
    {
        return new self(Uuid::uuid4()->toString());
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }


}