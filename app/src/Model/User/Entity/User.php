<?php


namespace App\Model\User\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_user")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{


    /**
     * @ORM\Id
     * @ORM\Column(type="user_id", length=255,  unique=true)
     */
    private $id;
    /**
     * @ORM\Column(type="string", unique=true, length=100 )
     */
    private $email;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lastName;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    function __construct(UserId $id, $email)
    {
        $this->id = $id;
        $this->email = $email;
    }

    /**
     * @return UserId
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }




    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function setPasswordHash(string  $password)
    {
        $this->password = $password;
    }
    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->getEmail();
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @ORM\PrePersist()
     */
    public function onCreate(){
        $date = new \DateTimeImmutable();
        $this->created_at = $date;
        $this->updated_at = $date;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onUpdate(){
        $this->updated_at = new \DateTime();
    }
}