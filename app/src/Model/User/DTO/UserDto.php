<?php


namespace App\Model\User\DTO;


class UserDto
{

    public $id;
    public $email;
    public $lastName;
    public $firstName;

}