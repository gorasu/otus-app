<?php


namespace App\Tests\Functional\Controller\V1;


use App\Model\User\Entity\User;
use App\Model\User\Service\CreateUser;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class UserControllerTest extends WebTestCase
{

    function testIndex(){

        $client = static::createClient();

        $client->request('GET', '/v1/user/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());


    }

    function testCreate(){

        $client = static::createClient();
        $faker = Factory::create('ru');

        $email = $faker->email;
        $res = $client->request('POST', '/v1/user/create', [
            'email'=> $email
            , 'firstName'=>$faker->firstName
            , 'lastName'=>$faker->lastName
            , 'password'=>$faker->password
        ]);
        $user =  $client->getContainer()->get('doctrine')->getRepository(User::class)->findOneBy(['email'=>$email]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals($user->getId()->getValue(), json_decode($client->getResponse()->getContent())->id);

    }
    function testGet(){

        $client = static::createClient();
        $faker = Factory::create('ru');

        $createUser = $this->getCreateUserService($client->getContainer());
        $email =$faker->email;
        $createUser->fromArray([
            'email' => $email
            ,'firstName' => $faker->firstName
            ,'lastName' => $faker->lastName
            , 'password'=>$faker->password

        ]);
        /** @var User $user */
        $user =  $client->getContainer()->get('doctrine')->getRepository(User::class)->findOneBy(['email'=>$email]);

        $client->request('GET', '/v1/user/'.$user->getId());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals($user->getId(), json_decode($client->getResponse()->getContent())->id);

    }

    function testCredentials(){

        $client = static::createClient();
        $faker = Factory::create('ru');

        $createUser = $this->getCreateUserService($client->getContainer());
        $email =$faker->email;
        $password = $faker->password;
        $createUser->fromArray([
            'email' => $email
            ,'firstName' => $faker->firstName
            ,'lastName' => $faker->lastName
            , 'password'=>$password

        ]);
        /** @var User $user */
        $user =  $client->getContainer()->get('doctrine')->getRepository(User::class)->findOneBy(['email'=>$email]);

        $client->request('POST', '/v1/user/credentials', [
            'login'=>$email
            ,'password'=>$password
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals($user->getId(), json_decode($client->getResponse()->getContent())->id);

    }

    function testCredentialsFail(){

        $client = static::createClient();
        $faker = Factory::create('ru');

        $createUser = $this->getCreateUserService($client->getContainer());
        $email =$faker->email;
        $password = $faker->password;
        $createUser->fromArray([
            'email' => $email
            ,'firstName' => $faker->firstName
            ,'lastName' => $faker->lastName
            , 'password'=>$password

        ]);

        $client->request('POST', '/v1/user/credentials', [
            'login'=>$email
            ,'password'=>$faker->password
        ]);

        $this->assertEquals(500, $client->getResponse()->getStatusCode());

    }

    function testUpdate(){

        $client = static::createClient();
        $faker = Factory::create('ru');

        $createUser = $this->getCreateUserService($client->getContainer());
        $email =$faker->email;
        $createUser->fromArray([
            'email' => $email
            ,'firstName' => $faker->firstName
            ,'lastName' => $faker->lastName
            ,'password' => $faker->password

        ]);
        /** @var User $user */
        $user =  $client->getContainer()->get('doctrine')->getRepository(User::class)->findOneBy(['email'=>$email]);

        $firstName = $faker->firstName;
        $lastName = $faker->lastName;
        $client->request('PUT', '/v1/user/update/'.$user->getId(), [
            'lastName'=>$lastName
            ,'firstName'=>$firstName
        ]);


        $user =  $client->getContainer()->get('doctrine')->getRepository(User::class)->find($user->getId());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertEquals($user->getFirstName(), $firstName);
        $this->assertEquals($user->getLastName(), $lastName);

    }


     function getCreateUserService (ContainerInterface $container){


         $passwordEncoder = $container->get('security.password_encoder');

          return  new CreateUser(
             $container->get('doctrine')->getManager(),
             $passwordEncoder
         );


    }
}